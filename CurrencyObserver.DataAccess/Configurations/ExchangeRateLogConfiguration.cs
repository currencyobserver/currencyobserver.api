﻿using System;
using System.Collections.Generic;
using CurrencyObserver.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CurrencyObserver.DataAccess.Configurations
{
    public class ExchangeRateLogConfiguration  : IEntityTypeConfiguration<ExchangeRateLog>
    {
        private readonly int _maxCurrencyCodeLength;

        public ExchangeRateLogConfiguration(int maxCurrencyCodeLength)
        {
            _maxCurrencyCodeLength = maxCurrencyCodeLength;
        }

        public void Configure(EntityTypeBuilder<ExchangeRateLog> builder)
        {
            builder.HasKey(log => log.Id);

            builder.Property(rate => rate.Source)
                .HasMaxLength(_maxCurrencyCodeLength)
                .IsRequired();

            builder.Property(rate => rate.Destination)
                .HasMaxLength(_maxCurrencyCodeLength)
                .IsRequired();

            builder.Property(rate => rate.DailyChangeInPercentage)
                .IsRequired();

            builder.HasOne(log => log.ExchangeRate)
                .WithMany(rate => rate.Logs)
                .HasForeignKey(log => log.ExchangeRateId)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasData(DummyExchangeRateLogs());
        }

        private static IEnumerable<ExchangeRateLog> DummyExchangeRateLogs()
        {
            yield return CreateRateLog("1", "1", "USD", "EUR", new DateTime(2021, 03, 19, 5, 4, 3));
            yield return CreateRateLog("2", "1", "USD", "EUR", new DateTime(2021, 03, 19, 5, 5, 3));
            yield return CreateRateLog("3", "2", "USD", "EUR", new DateTime(2021, 03, 20, 5, 4, 3));
            yield return CreateRateLog("4", "2", "USD", "EUR", new DateTime(2021, 03, 20, 5, 5, 3));
            yield return CreateRateLog("5", "3", "USD", "EUR", new DateTime(2021, 03, 21, 5, 4, 3));
            yield return CreateRateLog("6", "3", "USD", "EUR", new DateTime(2021, 03, 21, 5, 5, 3));
            yield return CreateRateLog("7", "4", "USD", "UAH", new DateTime(2021, 03, 20, 5, 4, 3));
            yield return CreateRateLog("8", "4", "USD", "UAH", new DateTime(2021, 03, 20, 5, 5, 3));
            yield return CreateRateLog("9", "5", "USD", "UAH", new DateTime(2021, 03, 21, 5, 4, 3));
            yield return CreateRateLog("10", "5", "USD", "UAH", new DateTime(2021, 03, 21, 5, 5, 3));
        }

        private static ExchangeRateLog CreateRateLog(string id, string rateId, string from, string to, DateTime date)
        {
            return new ExchangeRateLog
            {
                Id = id,
                Source = from,
                Destination = to,
                DailyChangeInPercentage = "+10%",
                UpdatedAtUtc = date,
                CurrentPrice = 1.2m,
                ExchangeRateId = rateId
            };
        }
    }
}
