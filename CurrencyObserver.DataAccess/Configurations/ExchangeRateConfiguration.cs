﻿using System;
using System.Collections.Generic;
using CurrencyObserver.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CurrencyObserver.DataAccess.Configurations
{
    public class ExchangeRateConfiguration : IEntityTypeConfiguration<ExchangeRate>
    {
        private readonly int _maxCurrencyCodeLength;

        public ExchangeRateConfiguration(int maxCurrencyCodeLength)
        {
            _maxCurrencyCodeLength = maxCurrencyCodeLength;
        }

        public void Configure(EntityTypeBuilder<ExchangeRate> builder)
        {
            builder.HasKey(rate => rate.Id);

            builder.Property(rate => rate.Source)
                .HasMaxLength(_maxCurrencyCodeLength)
                .IsRequired();

            builder.Property(rate => rate.Destination)
                .HasMaxLength(_maxCurrencyCodeLength)
                .IsRequired();

            builder.Property(rate => rate.DailyChangeInPercentage)
                .IsRequired();

            builder.HasIndex(rate => new { rate.Source, rate.Destination });

            builder.HasIndex(rate => rate.UpdatedAtUtc);

            builder.HasMany(rate => rate.Logs)
                .WithOne(log => log.ExchangeRate);

            builder.HasData(DummyExchangeRates());
        }

        private static IEnumerable<ExchangeRate> DummyExchangeRates()
        {
            yield return CreateRate("1", "USD", "EUR", new DateTime(2021, 03, 19));
            yield return CreateRate("2", "USD", "EUR", new DateTime(2021, 03, 20));
            yield return CreateRate("3", "USD", "EUR", new DateTime(2021, 03, 21));
            yield return CreateRate("4", "USD", "UAH", new DateTime(2021, 03, 20));
            yield return CreateRate("5", "USD", "UAH", new DateTime(2021, 03, 21));
        }

        private static ExchangeRate CreateRate(string id, string from, string to, DateTime date)
        {
            return new ExchangeRate
            {
                Id = id,
                Source = from,
                Destination = to,
                DailyChangeInPercentage = "+10%",
                UpdatedAtUtc = date,
                CurrentPrice = 1.2m,
                HighestPrice = 1.3m,
                LowestPrice = 1.1m
            };
        }
    }
}
