﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CurrencyObserver.DataAccess.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ExchangeRates",
                columns: table => new
                {
                    Id = table.Column<string>(type: "varchar(767)", nullable: false),
                    HighestPrice = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    LowestPrice = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    Source = table.Column<string>(type: "varchar(3)", maxLength: 3, nullable: false),
                    Destination = table.Column<string>(type: "varchar(3)", maxLength: 3, nullable: false),
                    CurrentPrice = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    DailyChangeInPercentage = table.Column<string>(type: "text", nullable: false),
                    UpdatedAtUtc = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExchangeRates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ExchangeRateLogs",
                columns: table => new
                {
                    Id = table.Column<string>(type: "varchar(767)", nullable: false),
                    ExchangeRateId = table.Column<string>(type: "varchar(767)", nullable: true),
                    Source = table.Column<string>(type: "varchar(3)", maxLength: 3, nullable: false),
                    Destination = table.Column<string>(type: "varchar(3)", maxLength: 3, nullable: false),
                    CurrentPrice = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    DailyChangeInPercentage = table.Column<string>(type: "text", nullable: false),
                    UpdatedAtUtc = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExchangeRateLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExchangeRateLogs_ExchangeRates_ExchangeRateId",
                        column: x => x.ExchangeRateId,
                        principalTable: "ExchangeRates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.InsertData(
                table: "ExchangeRates",
                columns: new[] { "Id", "CurrentPrice", "DailyChangeInPercentage", "Destination", "HighestPrice", "LowestPrice", "Source", "UpdatedAtUtc" },
                values: new object[,]
                {
                    { "1", 1.2m, "+10%", "EUR", 1.3m, 1.1m, "USD", new DateTime(2021, 3, 19, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { "2", 1.2m, "+10%", "EUR", 1.3m, 1.1m, "USD", new DateTime(2021, 3, 20, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { "3", 1.2m, "+10%", "EUR", 1.3m, 1.1m, "USD", new DateTime(2021, 3, 21, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { "4", 1.2m, "+10%", "UAH", 1.3m, 1.1m, "USD", new DateTime(2021, 3, 20, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { "5", 1.2m, "+10%", "UAH", 1.3m, 1.1m, "USD", new DateTime(2021, 3, 21, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "ExchangeRateLogs",
                columns: new[] { "Id", "CurrentPrice", "DailyChangeInPercentage", "Destination", "ExchangeRateId", "Source", "UpdatedAtUtc" },
                values: new object[,]
                {
                    { "1", 1.2m, "+10%", "EUR", "1", "USD", new DateTime(2021, 3, 19, 5, 4, 3, 0, DateTimeKind.Unspecified) },
                    { "2", 1.2m, "+10%", "EUR", "1", "USD", new DateTime(2021, 3, 19, 5, 5, 3, 0, DateTimeKind.Unspecified) },
                    { "3", 1.2m, "+10%", "EUR", "2", "USD", new DateTime(2021, 3, 20, 5, 4, 3, 0, DateTimeKind.Unspecified) },
                    { "4", 1.2m, "+10%", "EUR", "2", "USD", new DateTime(2021, 3, 20, 5, 5, 3, 0, DateTimeKind.Unspecified) },
                    { "5", 1.2m, "+10%", "EUR", "3", "USD", new DateTime(2021, 3, 21, 5, 4, 3, 0, DateTimeKind.Unspecified) },
                    { "6", 1.2m, "+10%", "EUR", "3", "USD", new DateTime(2021, 3, 21, 5, 5, 3, 0, DateTimeKind.Unspecified) },
                    { "7", 1.2m, "+10%", "UAH", "4", "USD", new DateTime(2021, 3, 20, 5, 4, 3, 0, DateTimeKind.Unspecified) },
                    { "8", 1.2m, "+10%", "UAH", "4", "USD", new DateTime(2021, 3, 20, 5, 5, 3, 0, DateTimeKind.Unspecified) },
                    { "9", 1.2m, "+10%", "UAH", "5", "USD", new DateTime(2021, 3, 21, 5, 4, 3, 0, DateTimeKind.Unspecified) },
                    { "10", 1.2m, "+10%", "UAH", "5", "USD", new DateTime(2021, 3, 21, 5, 5, 3, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExchangeRateLogs_ExchangeRateId",
                table: "ExchangeRateLogs",
                column: "ExchangeRateId");

            migrationBuilder.CreateIndex(
                name: "IX_ExchangeRates_Source_Destination",
                table: "ExchangeRates",
                columns: new[] { "Source", "Destination" });

            migrationBuilder.CreateIndex(
                name: "IX_ExchangeRates_UpdatedAtUtc",
                table: "ExchangeRates",
                column: "UpdatedAtUtc");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExchangeRateLogs");

            migrationBuilder.DropTable(
                name: "ExchangeRates");
        }
    }
}
