﻿using CurrencyObserver.DataAccess.Configurations;
using CurrencyObserver.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace CurrencyObserver.DataAccess
{
    public sealed class CurrencyObserverDbContext : DbContext
    {
        private const int MaxCurrencyCodeLength = 3;

        public CurrencyObserverDbContext(DbContextOptions options) : base(options)
        {
            Database.Migrate();
        }

        public DbSet<ExchangeRate> ExchangeRates { get; set; }

        public DbSet<ExchangeRateLog> ExchangeRateLogs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLoggerFactory(
                    LoggerFactory.Create(builder => builder
                        .AddConsole()
                        .AddFilter(level => level >= LogLevel.Information)))
                .EnableSensitiveDataLogging()
                .EnableDetailedErrors();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ExchangeRateConfiguration(MaxCurrencyCodeLength));

            modelBuilder.ApplyConfiguration(new ExchangeRateLogConfiguration(MaxCurrencyCodeLength));
        }
    }
}
