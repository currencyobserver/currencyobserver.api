﻿namespace CurrencyObserver.DataAccess.Entities
{
    public class ExchangeRateLog : ExchangeRateBase
    {
        public string ExchangeRateId { get; set; }

        public ExchangeRate ExchangeRate { get; set; }
    }
}
