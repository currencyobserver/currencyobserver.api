﻿using System;

namespace CurrencyObserver.DataAccess.Entities
{
    public abstract class ExchangeRateBase
    {
        public string Id { get; set; }

        public string Source { get; set; }

        public string Destination { get; set; }

        public decimal CurrentPrice { get; set; }

        public string DailyChangeInPercentage { get; set; }

        public DateTime UpdatedAtUtc { get; set; }
    }
}
