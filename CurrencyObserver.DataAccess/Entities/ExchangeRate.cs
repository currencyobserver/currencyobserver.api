﻿using System.Collections.Generic;

namespace CurrencyObserver.DataAccess.Entities
{
    public class ExchangeRate : ExchangeRateBase
    {
        public decimal HighestPrice { get; set; }

        public decimal LowestPrice { get; set; }

        public ICollection<ExchangeRateLog> Logs { get; set; }
    }
}
