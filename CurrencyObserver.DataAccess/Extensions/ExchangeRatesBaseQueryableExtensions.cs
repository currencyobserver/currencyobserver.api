﻿using System;
using System.Linq;
using System.Linq.Expressions;
using CurrencyObserver.DataAccess.Entities;

namespace CurrencyObserver.DataAccess.Extensions
{
    public static class ExchangeRatesBaseQueryableExtensions
    {
        public static IQueryable<T> FilterByCurrency<T>(
            this IQueryable<T> exchangeRates,
            string source,
            string destination) where T : ExchangeRateBase
        {
            Expression<Func<T, bool>> predicate = rate =>
                rate.Source == source.ToUpper() &&
                rate.Destination == destination.ToUpper();

            return exchangeRates.Where(predicate);
        }

        public static IQueryable<T> FilterByDateRange<T>(
            this IQueryable<T> queryable,
            DateTime from,
            DateTime to) where T : ExchangeRateBase
        {
            Expression<Func<T, bool>> predicate = rate =>
                rate.UpdatedAtUtc >= from && rate.UpdatedAtUtc <= to;

            return queryable.Where(predicate);
        }

        public static IQueryable<T> FilterByDay<T>(
            this IQueryable<T> queryable,
            DateTime day) where T : ExchangeRateBase
        {
            var date = day.Date;

            Expression<Func<T, bool>> predicate = rate =>
                rate.UpdatedAtUtc.Date >= date && rate.UpdatedAtUtc < date.AddDays(1);

            return queryable.Where(predicate);
        }
    }
}
