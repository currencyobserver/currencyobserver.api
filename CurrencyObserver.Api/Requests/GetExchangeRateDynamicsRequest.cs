﻿using System.ComponentModel.DataAnnotations;

namespace CurrencyObserver.Api.Requests
{
    public class GetExchangeRateDynamicsRequest : DateRangeRequest
    {
        [Required]
        public string SourceCode { get; set; }

        [Required]
        public string DestinationCode { get; set; }
    }
}
