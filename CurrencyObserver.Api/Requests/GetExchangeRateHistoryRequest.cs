﻿using System.ComponentModel.DataAnnotations;

namespace CurrencyObserver.Api.Requests
{
    public class GetExchangeRateHistoryRequest : DateRangeRequest
    {
        [Required]
        public string SourceCode { get; set; }

        [Required]
        public string DestinationCode { get; set; }
    }
}
