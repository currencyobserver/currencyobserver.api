﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CurrencyObserver.Api.Requests
{
    public abstract class DateRangeRequest : IValidatableObject
    {
        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (To > DateTime.UtcNow || From > DateTime.UtcNow)
                yield return new ValidationResult("Date cannot be greater than UTC now");

            if (To > From)
                yield return new ValidationResult($"{nameof(From)} cannot be greater than {nameof(To)}");
        }
    }
}
