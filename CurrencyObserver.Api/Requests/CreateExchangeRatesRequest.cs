﻿using System.Collections.Generic;

namespace CurrencyObserver.Api.Requests
{
    public class CreateExchangeRatesRequest
    {
        public IEnumerable<ExchangeRateSummaryRequest> ExchangeRates { get; set; }
    }
}
