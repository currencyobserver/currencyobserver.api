﻿using System.ComponentModel.DataAnnotations;

namespace CurrencyObserver.Api.Requests
{
    public class GetExchangeRateReportRequest : DateRangeRequest
    {
        [Required]
        public string SourceCode { get; set; }

        [Required]
        public string DestinationCode { get; set; }
    }
}
