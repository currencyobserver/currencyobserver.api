﻿using System;

namespace CurrencyObserver.Api.Requests
{
    public class ExchangeRateSummaryRequest
    {
        public string Source { get; set; }

        public string Destination { get; set; }

        public decimal HighestPrice { get; set; }

        public decimal LowestPrice { get; set; }

        public decimal LatestPrice { get; set; }

        public string DailyChangeInPercentage { get; set; }

        public DateTime LastUpdatedAtUtc { get; set; }

        private bool Equals(ExchangeRateSummaryRequest other)
        {
            return Source == other.Source &&
                   Destination == other.Destination;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != GetType())
                return false;

            return Equals((ExchangeRateSummaryRequest) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Source, Destination);
        }
    }
}
