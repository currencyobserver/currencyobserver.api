﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CurrencyObserver.Api.Requests
{
    public class GetExchangeSummaryRequest
    {
        public DateTime Day { get; set; }

        [Required]
        public string SourceCode { get; set; }

        [Required]
        public string DestinationCode { get; set; }
    }
}
