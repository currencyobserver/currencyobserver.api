﻿using System;
using CurrencyObserver.Api.Requests;
using CurrencyObserver.DataAccess.Entities;

namespace CurrencyObserver.Api.Extensions
{
    public static class ExchangeRateSummaryRequestExtensions
    {
        public static ExchangeRate ToExchangeRate(this ExchangeRateSummaryRequest self)
        {
            return new ExchangeRate
            {
                Id = Guid.NewGuid().ToString(),
                Source = self.Source,
                Destination = self.Destination,
                HighestPrice = self.HighestPrice,
                LowestPrice = self.LowestPrice,
                CurrentPrice = self.LatestPrice,
                DailyChangeInPercentage = self.DailyChangeInPercentage,
                UpdatedAtUtc = self.LastUpdatedAtUtc
            };
        }

        public static ExchangeRateLog ToExchangeRateLog(this ExchangeRateSummaryRequest self)
        {
            return new ExchangeRateLog
            {
                Id = Guid.NewGuid().ToString(),
                Source = self.Source,
                Destination = self.Destination,
                CurrentPrice = self.LatestPrice,
                UpdatedAtUtc = self.LastUpdatedAtUtc,
                DailyChangeInPercentage = self.DailyChangeInPercentage
            };
        }
    }
}
