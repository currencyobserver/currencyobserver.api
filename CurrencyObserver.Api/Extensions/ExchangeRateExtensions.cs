﻿using System.Linq;
using CurrencyObserver.Api.Responses;
using CurrencyObserver.Api.Responses.Models;
using CurrencyObserver.DataAccess.Entities;

namespace CurrencyObserver.Api.Extensions
{
    public static class ExchangeRateExtensions
    {
        public static ExchangeRateDynamicsResponse ToExchangeRateDynamicsResponse(this ExchangeRate self)
        {
            return new ExchangeRateDynamicsResponse
            {
                Day = self.UpdatedAtUtc.ToShortDateString(),
                DailyChangeInPercentage = self.DailyChangeInPercentage,
                LastUpdatedAt = self.UpdatedAtUtc,
                LastDailyRateValue = self.CurrentPrice,
                MaxDailyRateValue = self.HighestPrice,
                MinDailyRateValue = self.LowestPrice
            };
        }

        public static ExchangeRateHistoryDay ToExchangeRateHistoryDay(this ExchangeRate self)
        {
            return new ExchangeRateHistoryDay
            {
                Day = self.UpdatedAtUtc.ToShortDateString(),
                MaxDailyRateValue = self.HighestPrice,
                MinDailyRateValue = self.LowestPrice,
                LastUpdatedAt = self.UpdatedAtUtc,
                DailyChangeInPercentage = self.DailyChangeInPercentage,
                CurrentRateValue = self.CurrentPrice,
                Values = self.Logs.Select(log => new CurrencyRateValue
                {
                    At = log.UpdatedAtUtc,
                    Price = log.CurrentPrice,
                    Dynamics = log.DailyChangeInPercentage
                })
            };
        }

        public static DailyExchangeRateResponse ToDailyExchangeRateResponse(this ExchangeRate self)
        {
            return new DailyExchangeRateResponse
            {
                MaxDailyRateValue = self.HighestPrice,
                MinDailyRateValue = self.LowestPrice,
                LastUpdatedAt = self.UpdatedAtUtc,
                LastDailyRateValue = self.CurrentPrice,
                DailyChangeInPercentage = self.DailyChangeInPercentage
            };
        }
    }
}
