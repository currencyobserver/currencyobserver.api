﻿using System;

namespace CurrencyObserver.Api.Responses.Models
{
    public class CurrencyRateValue
    {
        public decimal Price { get; set; }

        public DateTime At { get; set; }

        public string Dynamics { get; set; }
    }
}
