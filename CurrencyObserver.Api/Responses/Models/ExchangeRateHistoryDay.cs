﻿using System;
using System.Collections.Generic;

namespace CurrencyObserver.Api.Responses.Models
{
    public class ExchangeRateHistoryDay
    {
        public string Day { get; set; }

        public decimal MinDailyRateValue { get; set; }

        public decimal MaxDailyRateValue { get; set; }

        public decimal CurrentRateValue { get; set; }

        public DateTime LastUpdatedAt { get; set; }

        public string DailyChangeInPercentage { get; set; }

        public IEnumerable<CurrencyRateValue> Values { get; set; }
    }
}
