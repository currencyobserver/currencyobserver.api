﻿using System;

namespace CurrencyObserver.Api.Responses
{
    public class DailyExchangeRateResponse
    {
        public decimal MinDailyRateValue { get; set; }

        public decimal MaxDailyRateValue { get; set; }

        public decimal LastDailyRateValue { get; set; }

        public DateTime LastUpdatedAt { get; set; }

        public string DailyChangeInPercentage { get; set; }
    }
}
