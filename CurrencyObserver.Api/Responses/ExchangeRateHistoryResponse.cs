﻿using System.Collections.Generic;
using CurrencyObserver.Api.Responses.Models;

namespace CurrencyObserver.Api.Responses
{
    public class ExchangeRateHistoryResponse
    {
        public ICollection<ExchangeRateHistoryDay> History { get; set; }
    }
}
