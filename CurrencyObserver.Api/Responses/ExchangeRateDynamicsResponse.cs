﻿using System;

namespace CurrencyObserver.Api.Responses
{
    public class ExchangeRateDynamicsResponse
    {
        public string Day { get; set; }

        public decimal MinDailyRateValue { get; set; }

        public decimal MaxDailyRateValue { get; set; }

        public decimal LastDailyRateValue { get; set; }

        public DateTime LastUpdatedAt { get; set; }

        public string DailyChangeInPercentage { get; set; }
    }
}
