using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using CurrencyObserver.Api.Services;
using CurrencyObserver.Api.Services.Interfaces;
using CurrencyObserver.Api.Services.Report;
using CurrencyObserver.Api.Services.Report.Interfaces;
using CurrencyObserver.DataAccess;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CurrencyObserver.Api
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<CurrencyObserverDbContext>(options =>
                options.UseMySQL(_configuration.GetConnectionString("CurrencyObserverDbContext")));

            services.AddControllers();
            services.AddSwaggerGen();

            services.AddScoped<IExchangeRateReader, ExchangeRateReader>();
            services.AddScoped<IExchangeRateCreator, ExchangeRateCreator>();

            services.AddScoped<IExchangeRatesReportBuilderProvider, ExchangeRatesReportBuilderProvider>();
            services.AddScoped<IExchangeRatesReportCsvWriter, ExchangeRatesReportCsvWriter>();
            services.AddScoped<ExchangeRatesCsvReportBuilder>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(builder => builder.MapControllers());
        }
    }
}
