﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using CurrencyObserver.Api.Requests;
using CurrencyObserver.Api.Services.Interfaces;
using CurrencyObserver.Api.Services.Report.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CurrencyObserver.Api.Controllers
{
    [Route("exchange-rate")]
    public class ExchangeRateController : ControllerBase
    {
        private readonly IExchangeRateReader _exchangeRateReader;
        private readonly IExchangeRateCreator _exchangeRateCreator;
        private readonly IExchangeRatesReportBuilderProvider _reportBuilderProvider;

        public ExchangeRateController(
            IExchangeRateReader currencyRateManager,
            IExchangeRateCreator exchangeRateCreator,
            IExchangeRatesReportBuilderProvider reportBuilderProvider)
        {
            _exchangeRateReader = currencyRateManager;
            _exchangeRateCreator = exchangeRateCreator;
            _reportBuilderProvider = reportBuilderProvider;
        }

        [HttpPost("batch")]
        public async Task<IActionResult> BatchCreate([FromBody] CreateExchangeRatesRequest request)
        {
            await _exchangeRateCreator.CreateAsync(request);

            return Ok();
        }

        [HttpGet("history")]
        public async Task<IActionResult> GetExchangeRateHistory(
            [FromQuery, Required] GetExchangeRateHistoryRequest request)
        {
            var history = await _exchangeRateReader.GetExchangeRateHistoryAsync(request);

            if (history == null)
                return NotFound();

            return Ok(history);
        }

        [HttpGet("dynamics")]
        public async Task<IActionResult> GetExchangeRateDynamics(
            [FromQuery, Required] GetExchangeRateDynamicsRequest request)
        {
            var dynamics = await _exchangeRateReader.GetExchangeRateDynamicsAsync(request);

            if (dynamics == null)
                return NotFound();

            return Ok(dynamics);
        }

        [HttpGet]
        public async Task<IActionResult> GetCurrencySummaryByDay(
            [FromQuery, Required] GetExchangeSummaryRequest request)
        {
            if (request.Day > DateTime.UtcNow)
                return BadRequest($"{nameof(request.Day)} cannot be greater than {DateTime.UtcNow}");

            var summary = await _exchangeRateReader.GetExchangeRateSummaryByDayAsync(request);

            if (summary == null)
                return NotFound();

            return Ok(summary);
        }

        [HttpGet("report")]
        public async Task<IActionResult> CreateExchangeRatesReport(
            [FromQuery] GetExchangeRateReportRequest request,
            [FromQuery] ReportFormat format)
        {
            var builder = _reportBuilderProvider.Get(format);

            var (fileName, data) = await builder.Build(request);

            return File(data, builder.ContentType, fileName);
        }
    }
}
