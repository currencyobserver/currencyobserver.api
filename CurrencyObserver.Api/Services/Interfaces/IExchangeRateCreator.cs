﻿using System.Threading.Tasks;
using CurrencyObserver.Api.Requests;

namespace CurrencyObserver.Api.Services.Interfaces
{
    public interface IExchangeRateCreator
    {
        Task CreateAsync(CreateExchangeRatesRequest request);
    }
}
