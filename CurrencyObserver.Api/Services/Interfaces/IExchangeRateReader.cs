﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CurrencyObserver.Api.Requests;
using CurrencyObserver.Api.Responses;

namespace CurrencyObserver.Api.Services.Interfaces
{
    public interface IExchangeRateReader
    {
        Task<DailyExchangeRateResponse> GetExchangeRateSummaryByDayAsync(GetExchangeSummaryRequest request);

        Task<ExchangeRateHistoryResponse> GetExchangeRateHistoryAsync(GetExchangeRateHistoryRequest request);

        Task<IEnumerable<ExchangeRateDynamicsResponse>> GetExchangeRateDynamicsAsync(
            GetExchangeRateDynamicsRequest request);
    }
}
