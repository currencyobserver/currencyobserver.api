﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CurrencyObserver.Api.Requests;
using CurrencyObserver.Api.Services.Report.Interfaces;
using CurrencyObserver.DataAccess;
using CurrencyObserver.DataAccess.Entities;
using CurrencyObserver.DataAccess.Extensions;
using Microsoft.EntityFrameworkCore;

namespace CurrencyObserver.Api.Services.Report
{
    public class ExchangeRatesCsvReportBuilder : IExchangeRatesReportBuilder
    {
        private readonly IExchangeRatesReportCsvWriter _csvWriter;
        private readonly DbSet<ExchangeRate> _exchangeRates;

        public ExchangeRatesCsvReportBuilder(
            CurrencyObserverDbContext dbContext,
            IExchangeRatesReportCsvWriter csvWriter)
        {
            _csvWriter = csvWriter;
            _exchangeRates = dbContext.ExchangeRates;

            ContentType = "text/csv";
        }

        public async Task<(string fileName, Stream data)> Build(GetExchangeRateReportRequest request)
        {
            var exchangeRates = await _exchangeRates
                .FilterByCurrency(request.SourceCode, request.DestinationCode)
                .FilterByDateRange(request.From, request.To)
                .ToListAsync();

            if (!exchangeRates.Any())
                throw new InvalidOperationException("Not found");

            var reportName = $"report-{request.From.ToShortDateString()}-{request.To.ToShortDateString()}.csv";
            var file = await _csvWriter.CreateReport(exchangeRates);
            file.Position = 0;

            return (reportName, file);
        }

        public string ContentType { get; }
    }
}
