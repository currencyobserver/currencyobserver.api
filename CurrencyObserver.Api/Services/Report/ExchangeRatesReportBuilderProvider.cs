﻿using System;
using CurrencyObserver.Api.Requests;
using CurrencyObserver.Api.Services.Report.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace CurrencyObserver.Api.Services.Report
{
    public class ExchangeRatesReportBuilderProvider : IExchangeRatesReportBuilderProvider
    {
        private readonly IServiceProvider _serviceProvider;

        public ExchangeRatesReportBuilderProvider(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IExchangeRatesReportBuilder Get(ReportFormat format)
        {
            return format switch
            {
                ReportFormat.Csv => _serviceProvider.GetRequiredService<ExchangeRatesCsvReportBuilder>(),
                _ => throw new ArgumentOutOfRangeException(nameof(format), format, "Unsupported format type")
            };
        }
    }
}
