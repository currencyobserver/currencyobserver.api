﻿using CurrencyObserver.Api.Requests;

namespace CurrencyObserver.Api.Services.Report.Interfaces
{
    public interface IExchangeRatesReportBuilderProvider
    {
        IExchangeRatesReportBuilder Get(ReportFormat format);
    }
}
