﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CurrencyObserver.DataAccess.Entities;

namespace CurrencyObserver.Api.Services.Report.Interfaces
{
    public interface IExchangeRatesReportWriter
    {
        Task<Stream> CreateReport(IEnumerable<ExchangeRate> exchangeRates);
    }
}
