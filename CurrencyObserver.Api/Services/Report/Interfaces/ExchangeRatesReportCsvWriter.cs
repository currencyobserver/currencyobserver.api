﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CurrencyObserver.DataAccess.Entities;

namespace CurrencyObserver.Api.Services.Report.Interfaces
{
    public class ExchangeRatesReportCsvWriter : IExchangeRatesReportCsvWriter
    {
        public async Task<Stream> CreateReport(IEnumerable<ExchangeRate> exchangeRates)
        {
            var memoryStream = new MemoryStream();
            await using var writer = new StreamWriter(memoryStream, Encoding.UTF8, 256, true);
            await using var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture);

            await WriteHeader(csvWriter);

            foreach (var exchangeRate in exchangeRates)
            {
                await WriteRow(csvWriter, exchangeRate);
            }

            await writer.FlushAsync();

            return memoryStream;
        }

        private static async Task WriteHeader(CsvWriter csvWriter)
        {
            csvWriter.WriteField(nameof(ExchangeRate.Source));
            csvWriter.WriteField(nameof(ExchangeRate.Destination));
            csvWriter.WriteField(nameof(ExchangeRate.CurrentPrice));
            csvWriter.WriteField(nameof(ExchangeRate.HighestPrice));
            csvWriter.WriteField(nameof(ExchangeRate.LowestPrice));
            csvWriter.WriteField(nameof(ExchangeRate.UpdatedAtUtc));
            csvWriter.WriteField(nameof(ExchangeRate.DailyChangeInPercentage));

            await csvWriter.NextRecordAsync();
        }

        private static async Task WriteRow(CsvWriter csvWriter, ExchangeRate exchangeRate)
        {
            csvWriter.WriteField(exchangeRate.Source);
            csvWriter.WriteField(exchangeRate.Destination);
            csvWriter.WriteField(exchangeRate.CurrentPrice);
            csvWriter.WriteField(exchangeRate.HighestPrice);
            csvWriter.WriteField(exchangeRate.LowestPrice);
            csvWriter.WriteField(exchangeRate.UpdatedAtUtc.ToString("yyyy-MM-dd HH:mm:ss"));
            csvWriter.WriteField(exchangeRate.DailyChangeInPercentage);

            await csvWriter.NextRecordAsync();
        }
    }
}
