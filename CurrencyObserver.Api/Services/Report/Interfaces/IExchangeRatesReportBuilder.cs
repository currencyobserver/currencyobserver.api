﻿using System.IO;
using System.Threading.Tasks;
using CurrencyObserver.Api.Requests;

namespace CurrencyObserver.Api.Services.Report.Interfaces
{
    public interface IExchangeRatesReportBuilder
    {
        Task<(string fileName, Stream data)> Build(GetExchangeRateReportRequest request);

        string ContentType { get; }
    }
}
