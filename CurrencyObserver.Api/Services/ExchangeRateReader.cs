﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CurrencyObserver.Api.Extensions;
using CurrencyObserver.Api.Requests;
using CurrencyObserver.Api.Responses;
using CurrencyObserver.Api.Services.Interfaces;
using CurrencyObserver.DataAccess;
using CurrencyObserver.DataAccess.Entities;
using CurrencyObserver.DataAccess.Extensions;
using Microsoft.EntityFrameworkCore;

namespace CurrencyObserver.Api.Services
{
    public class ExchangeRateReader : IExchangeRateReader
    {
        private readonly DbSet<ExchangeRate> _exchangeRatesDbSet;

        public ExchangeRateReader(CurrencyObserverDbContext dbContext)
        {
            _exchangeRatesDbSet = dbContext.ExchangeRates;
        }

        public async Task<DailyExchangeRateResponse> GetExchangeRateSummaryByDayAsync(GetExchangeSummaryRequest request)
        {
            if (request.Day > DateTime.UtcNow)
                throw new InvalidOperationException();

            var rate = await _exchangeRatesDbSet
                .AsNoTracking()
                .FilterByCurrency(request.SourceCode, request.DestinationCode)
                .FilterByDay(request.Day)
                .SingleAsync();

            var response = rate?.ToDailyExchangeRateResponse();

            return response;
        }

        public async Task<ExchangeRateHistoryResponse> GetExchangeRateHistoryAsync(
            GetExchangeRateHistoryRequest request)
        {
            var rates = await _exchangeRatesDbSet
                .AsNoTracking()
                .FilterByCurrency(request.SourceCode, request.DestinationCode)
                .FilterByDateRange(request.From, request.To)
                .Include(rate => rate.Logs)
                .OrderBy(rate => rate.UpdatedAtUtc.Date)
                .ToListAsync();

            if (rates == null || !rates.Any())
                return null;

            var historyResponse = new ExchangeRateHistoryResponse
            {
                History = rates.Select(rate => rate.ToExchangeRateHistoryDay()).ToList()
            };

            return historyResponse;
        }

        public async Task<IEnumerable<ExchangeRateDynamicsResponse>> GetExchangeRateDynamicsAsync(
            GetExchangeRateDynamicsRequest request)
        {
            var filteredRates = await _exchangeRatesDbSet
                .AsNoTracking()
                .FilterByCurrency(request.SourceCode, request.DestinationCode)
                .FilterByDateRange(request.From, request.To)
                .ToListAsync();

            return filteredRates.Select(rate => rate.ToExchangeRateDynamicsResponse());
        }
    }
}
