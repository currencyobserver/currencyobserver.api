﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CurrencyObserver.Api.Extensions;
using CurrencyObserver.Api.Requests;
using CurrencyObserver.Api.Services.Interfaces;
using CurrencyObserver.DataAccess;
using CurrencyObserver.DataAccess.Entities;
using CurrencyObserver.DataAccess.Extensions;
using Microsoft.EntityFrameworkCore;

namespace CurrencyObserver.Api.Services
{
    public class ExchangeRateCreator : IExchangeRateCreator
    {
        private readonly CurrencyObserverDbContext _dbContext;
        private readonly DbSet<ExchangeRate> _exchangeRatesDbSet;
        private readonly DbSet<ExchangeRateLog> _exchangeRateLogsDbSet;

        public ExchangeRateCreator(CurrencyObserverDbContext dbContext)
        {
            _dbContext = dbContext;
            _exchangeRatesDbSet = dbContext.ExchangeRates;
            _exchangeRateLogsDbSet = dbContext.ExchangeRateLogs;
        }

        public async Task CreateAsync(CreateExchangeRatesRequest request)
        {
            var exchangeRatesDate = request.ExchangeRates.First().LastUpdatedAtUtc.Date;

            ValidateRequest(request);

            var logs = new List<ExchangeRateLog>(request.ExchangeRates.Count());
            var newExchangeRates = new List<ExchangeRate>();

            var storedExchangeRates = await _exchangeRatesDbSet
                .FilterByDay(exchangeRatesDate)
                .ToDictionaryAsync(rate => (rate.Source, rate.Destination), rate => rate);

            foreach (var rate in request.ExchangeRates)
            {
                var exchangeKey = (rate.Source, rate.Destination);

                ExchangeRate storedRate;
                if (!storedExchangeRates.ContainsKey(exchangeKey))
                {
                    var newRate = rate.ToExchangeRate();
                    newRate.Id = Guid.NewGuid().ToString();
                    storedRate = newRate;
                    newExchangeRates.Add(storedRate);
                }
                else
                {
                    var storedExchange = storedExchangeRates[exchangeKey];
                    UpdateExchangeRate(storedExchange, rate);
                    storedRate = storedExchange;
                }

                var log = rate.ToExchangeRateLog();
                log.ExchangeRateId = storedRate.Id;
                logs.Add(log);
            }

            await _exchangeRateLogsDbSet.AddRangeAsync(logs);
            await _exchangeRatesDbSet.AddRangeAsync(newExchangeRates);

            await _dbContext.SaveChangesAsync();
        }

        private static void ValidateRequest(CreateExchangeRatesRequest request)
        {
            var exchangeRatesDate = request.ExchangeRates.First().LastUpdatedAtUtc.Date;

            if (request.ExchangeRates.Any(rate => rate.LastUpdatedAtUtc.Date > exchangeRatesDate))
                throw new InvalidOperationException("Cannot save data for a period of more than a day");

            var exchangeRateNames = new HashSet<(string source, string destination)>();

            if (request.ExchangeRates.Any(rate => !exchangeRateNames.Add((rate.Source, rate.Destination))))
                throw new InvalidOperationException("Payload can contain only distinct exchange names");
        }

        private static void UpdateExchangeRate(ExchangeRate exchangeRate, ExchangeRateSummaryRequest rateSummary)
        {
            exchangeRate.CurrentPrice = rateSummary.LatestPrice;
            exchangeRate.HighestPrice = rateSummary.HighestPrice;
            exchangeRate.LowestPrice = rateSummary.LowestPrice;
            exchangeRate.DailyChangeInPercentage = rateSummary.DailyChangeInPercentage;
            exchangeRate.UpdatedAtUtc = rateSummary.LastUpdatedAtUtc;
        }
    }
}
