﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CurrencyObserver.Api.Requests;
using CurrencyObserver.Api.Services;
using CurrencyObserver.DataAccess;
using CurrencyObserver.DataAccess.Entities;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace CurrencyObserver.Api.Tests
{
    [TestFixture]
    public class ExchangeRateCreatorTests
    {
        private CurrencyObserverDbContext _inMemoryDbContext;
        private ExchangeRateCreator _exchangeRateCreator;

        [SetUp]
        public async Task Setup()
        {
            _inMemoryDbContext = await CreateDbContext();

            _exchangeRateCreator = new ExchangeRateCreator(_inMemoryDbContext);
        }

        [TearDown]
        public async Task TearDown()
        {
            await CreateExchangeRateLogs();
        }

        [Test]
        public async Task CreateAsync_ThrowsException_WhenTriesToCreateRatesForPeriodOfMoreThanOneDay()
        {
            var request = new CreateExchangeRatesRequest
            {
                ExchangeRates = new List<ExchangeRateSummaryRequest>
                {
                    CreateExchangeRateSummaryRequest("USD", "EUR", new DateTime(2020, 01, 03)),
                    CreateExchangeRateSummaryRequest("EUR", "USD", new DateTime(2020, 01, 04))
                }
            };

            Func<Task> action = () => _exchangeRateCreator.CreateAsync(request);

            await action.Should().ThrowAsync<InvalidOperationException>();
        }

        [Test]
        public async Task CreateAsync_ThrowsException_WhenTriesToCreateRatesWithDuplicateExchangeNames()
        {
            var requestWithDuplicateNames = new CreateExchangeRatesRequest
            {
                ExchangeRates = new List<ExchangeRateSummaryRequest>
                {
                    CreateExchangeRateSummaryRequest("USD", "EUR", new DateTime(2020, 01, 03)),
                    CreateExchangeRateSummaryRequest("USD", "EUR", new DateTime(2020, 01, 03))
                }
            };

            Func<Task> action = () => _exchangeRateCreator.CreateAsync(requestWithDuplicateNames);

            await action.Should().ThrowAsync<InvalidOperationException>();
        }

        [Test]
        public async Task CreateAsync_CreatesRateLogsByRequestValues_WhenRequestIsValid()
        {
            var request = CreateValidRequest();

            await _exchangeRateCreator.CreateAsync(request);

            _inMemoryDbContext.ExchangeRateLogs.Should().HaveCount(request.ExchangeRates.Count());
        }

        [Test]
        public async Task CreateAsync_UpdatesStoredExchangeRates_WhenRatesWithSameNameAndDayAreFound()
        {
            var ratesCount = _inMemoryDbContext.ExchangeRates.Count();
            var request = CreateValidRequest();

            await _exchangeRateCreator.CreateAsync(request);

            _inMemoryDbContext.ExchangeRates.Should().HaveCount(ratesCount);
        }

        [Test]
        public async Task CreateAsync_CreatesExchangeRates_WhenRatesWithSameNameAndDayAreNotFound()
        {
            await CreateExchangeRates();
            var request = CreateValidRequest();

            await _exchangeRateCreator.CreateAsync(request);

            _inMemoryDbContext.ExchangeRates.Should().HaveCount(request.ExchangeRates.Count());
        }

        private CreateExchangeRatesRequest CreateValidRequest()
        {
            return new CreateExchangeRatesRequest
            {
                ExchangeRates = new List<ExchangeRateSummaryRequest>
                {
                    CreateExchangeRateSummaryRequest("USD", "EUR", new DateTime(2020, 01, 03)),
                    CreateExchangeRateSummaryRequest("USD", "UAH", new DateTime(2020, 01, 03))
                }
            };
        }

        private static async Task<CurrencyObserverDbContext> CreateDbContext()
        {
            var options = new DbContextOptionsBuilder<CurrencyObserverDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var databaseContext = new CurrencyObserverDbContext(options);

            var rates = new List<ExchangeRate>
            {
                CreateRate("1", "USD", "EUR", new DateTime(2021, 03, 19)),
                CreateRate("2", "USD", "EUR", new DateTime(2021, 03, 20)),
                CreateRate("3", "USD", "EUR", new DateTime(2021, 03, 21)),
                CreateRate("4", "USD", "UAH", new DateTime(2021, 03, 20)),
                CreateRate("5", "USD", "UAH", new DateTime(2021, 03, 21))
            };

            databaseContext.ExchangeRates.AddRange(rates);
            await databaseContext.SaveChangesAsync();
            return databaseContext;
        }

        private ExchangeRateSummaryRequest CreateExchangeRateSummaryRequest(string from, string to, DateTime date)
        {
            return new ExchangeRateSummaryRequest
            {
                Source = from,
                Destination = to,
                DailyChangeInPercentage = "-0.2%",
                HighestPrice = 1.10m,
                LowestPrice = 1,
                LatestPrice = 1.06m,
                LastUpdatedAtUtc = date
            };
        }

        private static ExchangeRate CreateRate(string id, string from, string to, DateTime date)
        {
            return new ExchangeRate
            {
                Id = id,
                Source = from,
                Destination = to,
                DailyChangeInPercentage = "+10%",
                UpdatedAtUtc = date,
                CurrentPrice = 1.12m,
                HighestPrice = 1.12m,
                LowestPrice = 1.12m
            };
        }

        private async Task CreateExchangeRates()
        {
            _inMemoryDbContext.ExchangeRates.RemoveRange(_inMemoryDbContext.ExchangeRates);
            await _inMemoryDbContext.SaveChangesAsync();
        }

        private async Task CreateExchangeRateLogs()
        {
            _inMemoryDbContext.ExchangeRateLogs.RemoveRange(_inMemoryDbContext.ExchangeRateLogs);
            await _inMemoryDbContext.SaveChangesAsync();
        }
    }
}
